
import { createStore, compose,applyMiddleware} from 'redux'
import thunk from "redux-thunk";
import { persistStore} from 'redux-persist';

import reducer from '../reducers';
const store = compose(applyMiddleware(thunk))(createStore)(reducer)
persistStore(store);

export default store;