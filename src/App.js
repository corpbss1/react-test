import React, { Component } from 'react';
import DatePicker from 'react-datepicker';
import moment from 'moment';
import logo from './logo.svg';
import 'react-datepicker/dist/react-datepicker.css';
import './App.css'
import 'bootstrap/dist/css/bootstrap.css';
import { Button, Form, FormGroup, Label, Input, FormText } from 'reactstrap';

import  { connect } from 'react-redux';
import * as Questions from './actions/questions';
import _ from 'underscore';

class FilterForm extends Component {
     constructor(props){
          super(props);
          this.state = {
               data: this.props.data,
               questions : [],
               question:{},
               selected:"",
               fullGiven : false,
               results : 0,
               alreadyGiven: false
          }
     }
     resetPass(){
      console.log("Get data",this.props);
      this.props.getQuestions().then((data,err) => {
        console.log(data,err);
        if(data && data.data && data.data.results)
         this.setState({questions:data.data.results,question:0,results:0,fullGiven:false})
      })
     }

     submit(){
      console.log("Submit",this.props);
      let newQuestions = this.state.questions;
      newQuestions[this.state.question]["given"] = true;
      newQuestions[this.state.question]["selceted"] = this.state.selected;
      let objToChange ={questions:newQuestions, selected : ""};
      if(this.state.question != 9) objToChange['question'] = this.state.question + 1
      this.setState(objToChange, () => {
        this.checkLast()
      });
     }

     checkLast(){
      if(_.where(this.state.questions,{given:true}).length == 10){
        console.log("Fully Given");
        let results = 0;
        _.filter(this.state.questions,(question) => {
          if(question.selceted == question.correct_answer){
            results = results + 1;
          }
        })
        this.setState({fullGiven:true,results:results,alreadyGiven:true})
      } 
     }

     setClicked(event){
      console.log(event.target.value);
      this.setState({selected:event.target.value})
     }
     render(){
          console.log('questions',this.state)
          return (
            <div>
              {(this.state.questions && this.state.questions.length && !this.state.fullGiven) && 

                <Form>
                  <FormGroup style={{"textAlign":'center',"marginTop":"50px"}}>
                    <Label style={{"fontSize":"18px"}}>{this.state.questions[this.state.question].question}</Label>
                  </FormGroup>

                   <FormGroup tag="fieldset" onChange={this.setClicked.bind(this)}>
                    <legend>Please select one</legend>
                    <FormGroup check>
                      <Label check style={{"fontSize":"14px"}}>
                        <Input type="radio" name="ques" value='True' checked={this.state.selected == 'True'} style={{"marginLeft":"-15px"}}/>{' '}
                        True
                      </Label>
                    </FormGroup>
                    <FormGroup check >
                      <Label check style={{"fontSize":"14px"}}>
                        <Input type="radio" name="ques"  value='False' checked={this.state.selected == 'False'} style={{"marginLeft":"-15px"}}/>{' '}
                        False
                      </Label>
                    </FormGroup>
                  </FormGroup>
                  <FormGroup style={{"textAlign":'center',"marginTop":"50px"}}>
                    <Button color="danger" size="lg" onClick={this.submit.bind(this)}>Submit</Button>
                  </FormGroup>
                </Form>
              }
              {(!this.state.questions || !this.state.questions.length) &&

               <div className="container" style={{"textAlign":'center',"marginTop":"50px"}}>
                  <Button color="danger" size="lg" onClick={this.resetPass.bind(this)}> Play Quiz </Button>
               </div>
             }

             {(this.state.fullGiven) && 
             <Form>
                  <FormGroup style={{"textAlign":'center',"marginTop":"50px"}}>
                    <Label style={{"fontSize":"18px"}}>Hyy, Your results are (out of 10): {this.state.results}</Label>
                  </FormGroup>
              </Form>
              }

              {(this.state.fullGiven && this.state.alreadyGiven) && 
                <div className="container" style={{"textAlign":'center',"marginTop":"50px"}}>
                  <Button color="danger" size="lg" onClick={this.resetPass.bind(this)}> Play Quiz Again</Button>
               </div>
              }

            </div>
          )
     }
}

class App extends Component {
     constructor(props){
          super(props)
     }
     render() {
          return (
               <FilterForm getQuestions={this.props.getQuestions}/>
          )
     }

}

const mapDispatchToProps = (dispatch) =>{
     return {
          getQuestions: () => dispatch(Questions.getQuestions())
     };
};
export default connect(null,mapDispatchToProps)(App);
