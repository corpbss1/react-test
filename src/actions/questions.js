import axios from "axios";

export function getQuestions(){
     return function(dispatch){
          return axios.get("https://opentdb.com/api.php?amount=10&type=boolean")
          .then(response => {
               return response;
          })
          .catch((error) => {
               return error; 
          });
     };
}
